import './components/index'
import { StackNavigation } from './services/stackNavigation'

import * as mapboxgl from 'mapbox-gl'
import cfg from './config'
(mapboxgl as any).accessToken = cfg.apiKey

StackNavigation.push(document.createElement('mapbox-map'))

window.oncontextmenu = (event) => {
    event.preventDefault()
    event.stopPropagation()
    return false
}

import runtime from 'serviceworker-webpack-plugin/lib/runtime'

if ('serviceWorker' in navigator) {
    runtime.register()
}

window.addEventListener('load', () => {
    if (!navigator.onLine) {
        StackNavigation.push(document.createElement('offline-modal'))
    }
    window.addEventListener('online', () => {
        StackNavigation.pop()
    })
    window.addEventListener('offline', () => {
        StackNavigation.push(document.createElement('offline-modal'))
    })
})
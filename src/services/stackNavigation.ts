import { createStack } from '../stack'

const stack = createStack<HTMLElement>()

export class StackNavigation {

    public static push(component: HTMLElement) {
        const page = document.createElement('stack-page')
        page.appendChild(component)
        stack.push(page)
        document.body.appendChild(page)
    }

    public static pop() {
        document.body.removeChild(stack.peek())
        stack.pop()
    }

}

declare module "*.html" {
    const template: string
    export default template
}

declare module "*.css" {
    const css: string
    export default css
}

declare module 'serviceworker-webpack-plugin/lib/runtime' {
    const sw: WebpackSWPluginRuntime 
    export default sw
}

interface WebpackSWPluginRuntime {
    register: () => void
}
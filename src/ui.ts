export function createElement(type: string, classes: string[], content?: string) {
    const el = document.createElement(type)
    el.className = classes.join(' ')
    if (content)
        el.innerText = content
    return el
}

export function createButtonControl(content: string, classes?: string[]): HTMLElement {
    const button = document.createElement('button')
    if (classes) {
        button.className = `button ${classes.join(' ')}`
    } else {
        button.className = 'button'
    }
    button.innerText = content

    return button
}

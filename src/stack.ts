export function createStack<T>() {

    const stack: T[] = []

    function push(el: T) {
        stack.push(el)
    }

    function pop() {
        stack.splice(-1, 1)
    }

    function peek() {
        return stack[stack.length - 1]
    }

    return {
        push, pop, peek,
    }

}

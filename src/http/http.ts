const backendUrl = 'http://localhost:3000/'

export class Http {
    public static get(url: string) {
        return request(url, 'GET')
    }

    public static post(url: string, body: any) {
        return request(url, 'POST', body)
    }

}


function request(url: string, method: string, body?: any) {
    return fetch(backendUrl + url, {
        method,
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify(body),
    }).then((x) => x.json())
}

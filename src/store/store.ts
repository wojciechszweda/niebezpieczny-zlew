import { LngLat } from 'mapbox-gl'
import createStore from 'unistore'

export interface Checkpoint {
    coordinates: LngLat
    description: string
    address?: string
    password: string
}

// tslint:disable-next-line:variable-name
export const Store = createStore<{ points: Checkpoint[], editMode: boolean }>({ points: [], editMode: false })
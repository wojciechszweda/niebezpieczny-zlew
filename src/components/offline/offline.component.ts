import template from './offline.html'

customElements.define('offline-modal',
    class extends HTMLElement {
        constructor() {
            super()
            const shadowRoot = this.attachShadow({ mode: 'open' })
            shadowRoot.innerHTML = template
        }
    })

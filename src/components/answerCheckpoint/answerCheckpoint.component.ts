import { StackNavigation } from '../../services/stackNavigation'
import { Checkpoint, Store } from '../../store/store'
import template from './answerCheckpoint.html'

export class AnswerCheckpoint extends HTMLElement {

    private root: ShadowRoot = this.attachShadow({ mode: 'open' })

    private _checkpoint: Checkpoint

    constructor() {
        super()
        this.root.innerHTML = template
        this.root.querySelector('form').addEventListener('submit', (ev) => {
            ev.preventDefault()
            const password = (this.root.getElementById('password') as HTMLInputElement).value
            if (password === this._checkpoint.password) {
                Store.setState({ points: Store.getState().points.filter((x) => x !== this._checkpoint) })
                StackNavigation.pop()
            } else {
                this.root.getElementById('error').textContent = 'Złe hasło'
            }
        })
    }

    set checkpoint(checkpoint: Checkpoint) {
        this._checkpoint = checkpoint
        this.root.getElementById('description').textContent = checkpoint.description
    }

    public connectedCallback() {
        this.root.getElementById('cancel').addEventListener('click', () => {
            StackNavigation.pop()
        })
    }
}

customElements.define('answer-checkpoint', AnswerCheckpoint)

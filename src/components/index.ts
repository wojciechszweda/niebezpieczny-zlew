declare const require: any
declare const __dirname: any

const req = require.context(__dirname, true, /.+\.component\.ts/)

req.keys().forEach((key: any) => {
    req(key)
})

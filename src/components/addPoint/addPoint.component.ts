import { LngLat } from 'mapbox-gl'
import cfg from '../../config'
import { Http } from '../../http/http'
import { StackNavigation } from '../../services/stackNavigation'
import { Store } from '../../store/store'
import template from './addPoint.html'

function reverseGeocoding(pos: LngLat) {
    const endpoint = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'
    return fetch(`${endpoint}${pos.lng},${pos.lat}.json?access_token=${cfg.apiKey}`)
        .then((x) => x.json())
}

export class AddPoint extends HTMLElement {

    private root: ShadowRoot = this.attachShadow({ mode: 'open' })

    private _point: LngLat

    private address: string

    constructor() {
        super()
        this.root.innerHTML = template
        this.root.querySelector('form').addEventListener('submit', (ev) => {
            ev.preventDefault()
            Http.post('points', Store.getState().points.concat({
                address: this.address,
                description: (this.root.getElementById('description') as HTMLInputElement).value,
                coordinates: this._point,
                password: (this.root.getElementById('password') as HTMLInputElement).value,
            })).then((points) => {
                Store.setState({ points })
                StackNavigation.pop()
            })
        })
    }

    set point(point: LngLat) {
        this._point = point
        reverseGeocoding(point)
            .then((res) => res.features[0].place_name)
            .then((address) => {
                this.address = address
                this.root.getElementById('address').innerText = address
            })
    }

    public connectedCallback() {
        this.root.getElementById('cancel').addEventListener('click', () => {
            StackNavigation.pop()
        })
    }
}

customElements.define('add-point', AddPoint)

import template from './stackPage.html'

customElements.define('stack-page',
    class extends HTMLElement {
        constructor() {
            super()
            const shadowRoot = this.attachShadow({ mode: 'open' })
            shadowRoot.innerHTML = template
        }
    })

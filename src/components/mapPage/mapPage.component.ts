import * as mapboxgl from 'mapbox-gl'
import cfg from '../../config'
import { MapZlew } from '../../map'
import { createButtonControl } from '../../ui'
import { AnswerCheckpoint } from './../answerCheckpoint/answerCheckpoint.component'
import template from './mapPage.html'

import { LngLat } from 'mapbox-gl'
import css from 'mapbox-gl/dist/mapbox-gl.css'
import { Unsubscribe } from 'unistore'
import { Http } from '../../http/http'
import { StackNavigation } from '../../services/stackNavigation'
import { Checkpoint, Store } from '../../store/store'
import { AddPoint } from '../addPoint/addPoint.component'


type Point2D = [number, number]

function distance(a: Point2D, b: Point2D) {
    return Math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)
}

function longPress(
    map: MapZlew,
    action: (e: mapboxgl.MapTouchEvent) => void,
) {

    let timeout: number
    let lastMove: TouchEvent
    let firstTouch: TouchEvent
    map.on('touchmove', (e: mapboxgl.MapTouchEvent) => {
        lastMove = e.originalEvent
    })

    map.on('touchstart', (e: mapboxgl.MapTouchEvent) => {
        firstTouch = e.originalEvent
        lastMove = undefined
        clearTimeout(timeout)
        timeout = setTimeout(() => {
            const p1: Point2D = [firstTouch.targetTouches[0].screenX, firstTouch.targetTouches[0].screenX]
            const p2: Point2D = lastMove ?
                [lastMove.targetTouches[0].screenX, lastMove.targetTouches[0].screenX]
                : p1
            if (distance(p1, p2) < 20) {
                action(e)
            }
        }, 500)
    })

    map.on('touchend', () => {
        clearTimeout(timeout)
    })

}


class MapPage extends HTMLElement {

    private root = this.attachShadow({ mode: 'open' })
    private unsubscribe: Unsubscribe
    private map: MapZlew

    constructor() {
        super()
        this.root.innerHTML = template
        const styleElement = document.createElement('style')
        styleElement.innerHTML = css
        this.root.appendChild(styleElement)

    }

    public disconnectedCallback() {
        if (this.unsubscribe) {
            this.unsubscribe()
        }
    }

    public connectedCallback() {

        StackNavigation.push(document.createElement('pick-mode'))
        this.map = new MapZlew({
            container: this.root.getElementById('map'),
            style: 'mapbox://styles/mapbox/streets-v10',
            center: cfg.startLocation,
            zoom: 14,
        })

        Http.get('points').then((points) => {
            Store.setState({ points: Array.from<Checkpoint>(points) })
        })

        this.unsubscribe = Store.subscribe(({ points }) => {
            if (Store.getState().editMode)
                this.map.setCheckpoints(points)
            else
                this.map.setCheckpoints(points[0] ? [points[0]] : [])
        })

        this.map.onCheckpointRemove = (checkpoint) => {
            if (Store.getState().editMode) {
                Http.post('points', Store.getState().points.filter((x) => x !== checkpoint))
                    .then((points) => {
                        Store.setState({ points })
                    })
            } else {
                // request
                const answerCheckpoint = document.createElement('answer-checkpoint') as AnswerCheckpoint
                answerCheckpoint.checkpoint = checkpoint
                StackNavigation.push(answerCheckpoint)
                // Store.setState({ points: Store.getState().points.filter((x) => x !== checkpoint) })
            }
        }

        const geolocate = new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true,
            },
            trackUserLocation: true,
        })

        this.map.on('load', () => {
            geolocate.on('geolocate', (event: Position) => {
                this.map.updateCurrentPosition(new LngLat(event.coords.longitude, event.coords.latitude))
            })

            this.map.addControl(geolocate, 'top-left')


            const btnPitch = createButtonControl('α°')
            btnPitch.onclick = this.pitchButtonClick.bind(this)

            const container = this.root.querySelector(`.mapboxgl-ctrl-top-left`)
            const group = document.createElement('div')
            group.className = 'mapboxgl-ctrl mapboxgl-ctrl-group'
            group.appendChild(btnPitch)
            container.appendChild(group)


            longPress(this.map, (event) => {
                if (Store.getState().editMode) {
                    const addPoint: AddPoint = (document.createElement('add-point') as any)
                    addPoint.point = event.lngLat
                    StackNavigation.push(addPoint)
                }
            })

            window.addEventListener('deviceorientation', this.handleOrientation.bind(this), false)
        })

    }

    private pitchButtonClick(event: MouseEvent) {
        this.map.isPitchMode = !this.map.isPitchMode
        const button: HTMLElement = event.target as HTMLElement
        if (this.map.isPitchMode) {
            button.className += ' active'
        } else {
            button.className = button.className.replace(' active', '')
            this.map.setPitch(0)
        }
    }

    private handleOrientation(event: DeviceOrientationEvent) {
        if (this.map.isPitchMode) {
            if (Math.abs(window.orientation as number) === 90) {
                const gamma = event.gamma ? event.gamma : 0
                this.map.setPitch(gamma * -Math.sign(window.orientation as number))
            } else {
                const beta = event.beta ? event.beta : 0
                const direction = window.orientation === 0 ? 1 : -1
                this.map.setPitch(beta * direction)
            }
        }
    }

}

customElements.define('mapbox-map', MapPage)

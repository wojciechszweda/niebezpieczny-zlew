import { Store } from '../../store/store'
import { StackNavigation } from './../../services/stackNavigation'
import template from './pickMode.html'

customElements.define('pick-mode',
    class extends HTMLElement {
        private root: ShadowRoot = this.attachShadow({ mode: 'open' })
        constructor() {
            super()
            this.shadowRoot.innerHTML = template

        }
        public connectedCallback() {
            this.root.getElementById('btnPlay').onclick = this.play
            this.root.getElementById('btnEdit').onclick = this.editMode
        }

        private editMode() {
            Store.setState({ editMode: true })
            StackNavigation.pop()
        }

        private play() {
            StackNavigation.pop()
        }
    })

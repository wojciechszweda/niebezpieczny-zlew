const CACHE_NAME = 'cache-v1'


self.addEventListener('install', async () => {
    const cache = await caches.open(CACHE_NAME)
    await cache.addAll(serviceWorkerOption.assets)
    return self.skipWaiting()
})

self.addEventListener('activate', () => {
    self.clients.claim()
})

self.addEventListener('fetch', async event => {
    const request = event.request
    const url = new URL(request.url)

    if (url.origin === location.origin) {
        event.respondWith(fromCache(request))
    } else if (request.destination === 'style') {
        event.respondWith(fromNetworkAndCache(request))
    } else {
        event.respondWith(fetch(request))
    }
})

async function fromCache(request) {
    const cache = await caches.open(CACHE_NAME)
    const cached = await cache.match(request)
    return cached || fetch(request)
}

async function fromNetworkAndCache(request) {
    const cache = await caches.open(CACHE_NAME)
    try {
        const newRequest = await fetch(request)
        await cache.put(request, newRequest.clone())
        return newRequest
    } catch (e) {
        const cached = await cache.match(request)
        return cached
    }
}
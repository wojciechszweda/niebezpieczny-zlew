import { Feature, GeoJsonProperties, LineString } from 'geojson'
import { LngLat } from 'mapbox-gl'
import * as mapboxgl from 'mapbox-gl'
import { Checkpoint, Store } from './store/store'
import { createElement } from './ui'

export class MapZlew extends mapboxgl.Map {
    public isPitchMode = false

    public onCheckpointRemove: (checkpoint: Checkpoint) => void

    private markers: mapboxgl.Marker[] = []
    private currentPosition: LngLat
    private currentPositionMarker: mapboxgl.Marker
    private lastPositionUpdateTime?: Date

    constructor(options?: mapboxgl.MapboxOptions) {
        super(options)
    }

    public setCheckpoints(checkpoints: Checkpoint[]) {
        this.clearMarkers()
        this.markers = checkpoints.map(this.createMarker.bind(this))
        this.updateRoute()
    }

    public updateCurrentPosition(coords: LngLat) {
        if (!this.lastPositionUpdateTime || this.lastPositionUpdateTime.getTime() + 10000 <= new Date().getTime()) {
            this.lastPositionUpdateTime = new Date()
            const marker = this.createCurrentPositionMarker(coords)
            this.currentPositionMarker = marker
            if (this.currentPositionMarker) {
                this.currentPositionMarker.remove()
            }
            marker.addTo(this)
            this.currentPosition = coords
            this.updateRoute()
        }
    }

    public async updateRoute() {
        const markersPositions = [
            ...this.currentPosition ? [this.currentPosition] : [],
            ...this.markers.map((x) => x.getLngLat()),
        ]
        if (markersPositions.length >= 2) {
            const layer = await this.getRouteLayer(markersPositions, 'route')
            this.replaceLayer('route', layer)
        } else {
            this.replaceLayer('route')
        }
    }

    private replaceLayer(layerId: string, layer?: mapboxgl.Layer) {
        if (this.getLayer('route'))
            this.removeLayer('route')
        if (this.getSource('route')) {
            this.removeSource('route')
        }
        if (layer)
            this.addLayer(layer)
    }

    private createCurrentPositionMarker(coords: LngLat): mapboxgl.Marker {
        const el = document.createElement('div')
        el.className = 'marker-current-position'
        const marker = new mapboxgl.Marker(el)
            .setLngLat(coords)
        return marker
    }

    private createMarker(checkpoint: Checkpoint) {
        const el = document.createElement('div')
        el.className = 'marker'
        const marker = new mapboxgl.Marker(el)
            .setLngLat(checkpoint.coordinates)

        const popupNode = this.createPopupNode(checkpoint)
        const popup = new mapboxgl.Popup({ offset: 25 })
            .setDOMContent(popupNode)

        marker.setPopup(popup).addTo(this)
        return marker
    }

    private createPopupNode(checkpoint: Checkpoint): HTMLElement {
        const descriptionNode = createElement('div', ['popup-description'], checkpoint.description)
        const editMode = Store.getState().editMode
        const button = createElement('button', ['button-popup'], editMode ? 'Usuń' : 'Znam hasło')
        button.addEventListener('click', () => {
            if (this.onCheckpointRemove) {
                this.onCheckpointRemove(checkpoint)
            }
        })
        const popupNode = createElement('div', ['popup-content'])
        popupNode.appendChild(descriptionNode)
        popupNode.appendChild(button)

        return popupNode
    }

    private clearMarkers() {
        for (const marker of this.markers) {
            marker.remove()
        }
    }

    private async getRouteLayer(checkpoints: LngLat[], layerId: string): Promise<mapboxgl.Layer> {
        const mode = 'walking'
        const baseUrl = `https://api.mapbox.com/directions/v5/mapbox/${mode}/`
        const coordinates = checkpoints.map((c) => `${c.lng},${c.lat}`).join(';')
        const url = `${baseUrl}${coordinates}?geometries=geojson&access_token=${mapboxgl.accessToken}`
        return await fetch(url)
            .then((response) => response.json())
            .then((data: any) => {
                const route: LineString = data.routes[0].geometry
                const sourceData: Feature<LineString, GeoJsonProperties> = {
                    geometry: route,
                    type: 'Feature',
                    properties: null,
                }
                const layer: mapboxgl.Layer = {
                    id: layerId,
                    type: 'line',
                    source: {
                        type: 'geojson',
                        data: sourceData,
                    },
                    paint: {
                        'line-width': 2,
                    },
                }
                return layer
            })
    }
}

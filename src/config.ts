import * as mapboxgl from 'mapbox-gl'

const config = {
    apiKey: 'pk.eyJ1IjoicXVhdG1vciIsImEiOiJjam1yenVwaWUwNGZpM3JvMTgzMmE3bDBnIn0.wJJcilXVyUFfF1jxQ5n65w',
    startLocation: new mapboxgl.LngLat(19.045119, 49.821841),
}

export default config